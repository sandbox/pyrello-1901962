(function($) {
	/**
   * @todo
   */	
	Drupal.behaviors.date_enhance = {
		attach: function (context) {
			$('.field-type-datetime div[id$="-value2"]').parent().each( function() {
				if ($(this).find('.date-remove-todate').length == 0) {
					$('.field-type-datetime div[id$="-value2"]').parent().append('<div class="date-remove-todate"><a class="date-remove-todate-link" href="#">x</a></div></div>');
					$('.date-remove-todate-link').click( function() {
						$(this).parents('.field-type-datetime').find('input[id$="-show-todate"]').attr('checked', false).change();
						return false;
					});
				}
			});
			$('input[name$="[value][date]"]').change( function() {
				if ($('input[name$="[value2][date]"]').val() == '') {
					$('input[name$="[value2][date]"]').val($(this).val())
				}
			});
			$('input[id$="-show-todate"]').change( function() {
				if ($(this).is(':checked') && $('input[name$="[value2][date]"]').val() == '') {
					$('input[name$="[value2][date]"]').val($('input[name$="[value][date]"]').val())
				}
			});
		}
	} // Drupal.behaviors.date_enhance
	
})(jQuery);